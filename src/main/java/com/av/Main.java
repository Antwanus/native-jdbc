package com.av;

import com.av.dom.Employee;
import com.av.dom.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

   public static void main(String[] args) {
      logProductById(1);
      insertProduct(new Product("product1", "1234567890123"));
      logProductById(1);
      insertEmployee(new Employee("hi", "there", "HR", 1000.00));

      procedureGiveDepartmentRaise("HR", 30.00);

      logAllEmployees();
   }


   private static final Logger LOGGER = Logger.getLogger( Main.class.getName() );
   private static final String URL = "jdbc:mysql://localhost:3306/db";
   private static final String USER = "app_client";
   private static final String PW = "app_client_pw";

   private static void procedureGiveDepartmentRaise(String department, double raise) {
      try(
          var conn = DriverManager.getConnection(URL, USER, PW);
          var cs = conn.prepareCall("{call increase_salary_for_department(?, ?)}")
      ) {
         cs.setString(1, department);
         cs.setDouble(2, raise);

         var bool = cs.execute();
         LOGGER.info(() -> Boolean.toString(bool));


      } catch (SQLException e) {
         e.printStackTrace();
      }
   }

   private static void insertProduct(Product p) {
      try (
          Connection con = DriverManager.getConnection(URL, USER, PW);
          PreparedStatement ps = con.prepareStatement("INSERT INTO products (name, ean) values (?, ?);")
      ) {
         ps.setString(1, p.getName());
         ps.setString(2, p.getEan());
         var rowsAffected = ps.executeUpdate();
         LOGGER.info(() -> "Rows updated: " + rowsAffected);
      } catch (SQLException ex) {
         ex.printStackTrace();
      }
   }

   private static void logAllEmployees() {
      List<Employee> employees = new ArrayList<>();
      try(
          var c = DriverManager.getConnection(URL, USER, PW);
          var s = c.createStatement()
      ){
         var result = s.executeQuery("SELECT id, first_name, last_name, department, salary, resume FROM employees AS e GROUP BY department;");
         while (result.next()) {
            employees.add(new Employee(
                result.getInt("id"),
                result.getString("first_name"),
                result.getString("last_name"),
                result.getString("department"),
                result.getDouble("salary"),
                result.getBlob("resume")
            ));
         }
         if (!employees.isEmpty()) LOGGER.info(employees::toString);
         else LOGGER.warning("NO RESULT FOUND");

      } catch (SQLException e) {
         e.printStackTrace();
      }
   }

   private static void insertEmployee(Employee e) {
      try (
          Connection con = DriverManager.getConnection(URL, USER, PW);
          PreparedStatement ps = con.prepareStatement("INSERT INTO employees (first_name, last_name, department, salary) values (?, ?, ?, ?);")
      ) {
         ps.setString(1, e.getFirstName());
         ps.setString(2, e.getLastName());
         ps.setString(3, e.getDepartment());
         ps.setDouble(4, e.getSalary());
         var rowsAffected = ps.executeUpdate();
         LOGGER.info(() -> "Rows updated: " + rowsAffected);
      } catch (SQLException ex) {
         ex.printStackTrace();
      }
   }

   private static void logProductById(int id) {
      List<Product> productList = new ArrayList<>();
      try (
          Connection con = DriverManager.getConnection(URL, USER, PW);
          PreparedStatement ps = con.prepareStatement("SELECT * FROM products WHERE id = ?")
      ) {
         ps.setInt(1, id);
         try (ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
               var pId = rs.getInt("id");
               var pName = rs.getString("name");
               var pEan = rs.getString("ean");
               productList.add(new Product(pId, pName, pEan));
            }
         }
      } catch (SQLException e) {
         e.printStackTrace();
      }
      try {
         LOGGER.info(()-> productList.get(0).toString());
      } catch (IndexOutOfBoundsException e) {
         LOGGER.log(Level.SEVERE, ()-> "The index is out of bounds!\n" + e);
      }
   }
}

