package com.av.blob;

import java.io.File;
import java.io.FileInputStream;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Logger;

public class BlobExample {
   public static void main(String[] args) {
      readProperties();
      writeBlob("./../../main/java/com/av/blob/2002-72203242.pdf");
   }

   private static final Logger LOGGER = Logger.getLogger( BlobExample.class.getName() );

   private static String pw;
   private static String url;
   private static String user;

   private static void readProperties() {
      Properties properties = new Properties();

      try(
          var file = new FileInputStream("src/main/resources/demo.properties");
      ){
         properties.load(file);
         url = properties.getProperty("dburl");
         user = properties.getProperty("user");
         pw = properties.getProperty("password");
         LOGGER.info(() -> url + ":" +  user + ":" + pw);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }



   private static void writeBlob(String filename) {
      String sql = "UPDATE employees SET resume=? WHERE id=1";
      File f = new File(filename);
      try(
          var c = DriverManager.getConnection(url, user, pw);
          var ps = c.prepareStatement(sql);
          FileInputStream inputStream = new FileInputStream(f)
      ) {
         ps.setBinaryStream(1, inputStream);
         ps.executeUpdate();

      } catch (Exception e) {
         e.printStackTrace();
      }
   }

}
