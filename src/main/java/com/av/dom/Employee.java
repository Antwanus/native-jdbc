package com.av.dom;

import java.sql.Blob;

public class Employee {
   private int id;
   private String firstName, lastName, department;
   private double salary;
   private Blob resume;

   public Employee(int id, String firstName, String lastName, String department, double salary) {
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.department = department;
      this.salary = salary;
      this.resume = resume;
   }
   public Employee(String firstName, String lastName, String department, double salary) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.department = department;
      this.salary = salary;
   }

   public Employee(int id, String firstName, String lastName, String department, double salary, Blob resume) {
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.department = department;
      this.salary = salary;
      this.resume = resume;
   }

   public int getId() {return id;}
   public void setId(int id) {this.id = id;}
   public String getFirstName() {return firstName;}
   public void setFirstName(String firstName) {this.firstName = firstName;}
   public String getLastName() {return lastName;}
   public void setLastName(String lastName) {this.lastName = lastName;}
   public String getDepartment() {return department;}
   public void setDepartment(String department) {this.department = department;}
   public double getSalary() {return salary;}
   public void setSalary(double salary) {this.salary = salary;}

   public Blob getResume() {
      return resume;
   }

   public void setResume(Blob resume) {
      this.resume = resume;
   }

   @Override
   public String toString() {
      return "\n\tEmployee{" +
          "id=" + id +
          ", firstName='" + firstName + '\'' +
          ", lastName='" + lastName + '\'' +
          ", department='" + department + '\'' +
          ", salary=" + salary + '\'' +
          ", cv=" + resume +
          "}";
   }
}
