package com.av.dom;

public class Product {
   private int id;
   private String name;
   private String ean;

   public Product(int id, String name, String ean) {
      this.id = id;
      this.name = name;
      this.ean = ean;
   }
   public Product(String name, String ean) {
      this.name = name;
      this.ean = ean;
   }

   public int getId() { return id; }
   public void setId(int id) { this.id = id; }
   public String getName() { return name; }
   public void setName(String name) { this.name = name; }
   public String getEan() { return ean; }
   public void setEan(String ean) { this.ean = ean; }

   @Override
   public String toString() {
      return "Product{" +
          "id=" + id +
          ", name='" + name + '\'' +
          ", ean='" + ean + '\'' +
          '}';
   }
}
