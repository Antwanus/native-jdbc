CREATE TABLE IF NOT EXISTS products (id INTEGER PRIMARY KEY AUTO_INCREMENT, name VARCHAR(55) NOT NULL, ean VARCHAR(55) NOT NULL, PRIMARY KEY (id));
CREATE TABLE IF NOT EXISTS employees (id INTEGER PRIMARY KEY AUTO_INCREMENT, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, department VARCHAR(50) NOT NULL, salary DECIMAL(8, 2));
ALTER TABLE employees ADD resume BLOB;