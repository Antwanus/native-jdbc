delimiter //
CREATE PROCEDURE increase_salary_for_department (IN lucky_department VARCHAR(55), IN bonus_amount DECIMAL(6, 2))
BEGIN
    UPDATE employees SET salary = salary + bonus_amount
    WHERE department = lucky_department;
END
//
delimiter ;

CALL increase_salary_for_department('stranger', 10.33);

show procedure status like '%sal%';